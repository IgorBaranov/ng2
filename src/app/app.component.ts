import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app',
    template: `
  <app-header></app-header>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <app-groups-list></app-groups-list>
            </div>
            <div class="col-md-9">
                <app-breadcrumbs></app-breadcrumbs>
                <router-outlet></router-outlet>
            </div>
        </div>
    </div>`
})
export class AppComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
    }

}
