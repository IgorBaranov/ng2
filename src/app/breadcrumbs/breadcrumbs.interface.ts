export interface BreadcrumbsItem {
    link?: string;
    name: string;
}