import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {BreadcrumbsItem} from './breadcrumbs.interface';
import {BreadcrumbsService} from './breadcrumbs.service';


@Component({
    selector: 'app-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {
    items: Array<BreadcrumbsItem>;

    constructor(
        private service: BreadcrumbsService,
        private router: Router
    ) {
        this.items = [service.firstItem];
    }

    ngOnInit() {
        this.service.items$.subscribe(subscriber => {
            this.items = [this.service.firstItem as BreadcrumbsItem].concat(subscriber);
        });
    }

    goToPage($event, item) {
        this.router.navigate([item.link]);
        $event.preventDefault();
    }
}
