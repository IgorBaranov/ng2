import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

import {BreadcrumbsItem} from './breadcrumbs.interface';

@Injectable()
export class BreadcrumbsService {
    public items = new Subject<Array<BreadcrumbsItem>>();
    public firstItem = {name: 'Home', link: '/'};
    public items$ = this.items.asObservable();

    setBreadcrumbs(data: Array<BreadcrumbsItem>){
        this.items.next(data);
    }
}
