export interface Cards {
    title: string;
    detail?: string;
    img?: string;
    inFavorites: boolean;
    link: string;
    groupID?: number;
}