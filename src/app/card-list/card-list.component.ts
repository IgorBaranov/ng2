import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';

import {CardServiceService} from '../card-service.service';
import {Cards} from '../card.interface';
import {BreadcrumbsService} from '../breadcrumbs/breadcrumbs.service';
import {GroupsService} from '../groups.service';


@Component({
    selector: 'app-card-list',
    templateUrl: './card-list.component.html',
    styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {
    items: Array<Cards>;

    constructor(
        private cards: CardServiceService,
        private route: ActivatedRoute,
        private breadcrumbs: BreadcrumbsService,
        private groups: GroupsService
        ) {
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            const id = params['id'] !== undefined ? +params['id'] : undefined;
            this.items = this.cards.getItems(id);
            let group = this.groups.getByID(+params['id']);
            let breadcumbsArray = [{name:'Cards', link: 'cards/'}];
            id !== undefined && (breadcumbsArray.push({name: group.title, link: '/'}));
            this.breadcrumbs.setBreadcrumbs(breadcumbsArray);
        });
    }

}