import { Injectable } from '@angular/core';

import { Cards } from './card.interface';

@Injectable()
export class CardServiceService {
    items: Array<Cards>;

    constructor() {
        this.items = [{
            title: 'test Title1',
            inFavorites: false,
            link: 'google.com',
            groupID: 0
        },{
            title: 'test Title2',
            inFavorites: false,
            link: 'google.com',
            detail: 'asdfjhasjkdfh akjshd fkjashdfkjhasdkjf kajsdfkljasjdfhlkasdhflkjahsdfkjhlakjsd',
            groupID: 1
        },{
            title: 'test Title3',
            inFavorites: true,
            link: 'google.com',
            groupID: 0
        },{
            title: 'test Title4',
            inFavorites: false,
            link: 'google.com',
            groupID: 2
        },{
            title: 'test Title5',
            inFavorites: false,
            link: 'google.com',
            detail: 'asdfjhasjkdfh akjshd fkjashdfkjhasdkjf kajsdfkljasjdfhlkasdhflkjahsdfkjhlakjsd',
            groupID: 0
        }];
    }

    getItems = (id: number) => id !== undefined ? this.items.filter(item => item.groupID === id) : this.items;

}
