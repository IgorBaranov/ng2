import {Component, OnInit} from '@angular/core';

import {GroupsService} from '../groups.service';

import { Group } from '../groups.interface';

@Component({
    selector: 'app-groups-list',
    templateUrl: './groups-list.component.html',
    styleUrls: ['./groups-list.component.css']
})
export class GroupsListComponent implements OnInit {
    items: Array<Group>;

    constructor(private GroupsService: GroupsService) {
        this.items = GroupsService.items;
    }

    ngOnInit() {
    }

}
