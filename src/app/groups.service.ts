import {Injectable} from '@angular/core';

import { Group } from './groups.interface';

@Injectable()
export class GroupsService {
    public items: Array<Group>;

    constructor() {
        this.items = [{
            title: 'Default',
            editable: false,
            locked: true,
            ID: 0
        }, {
            title: 'Group 1',
            editable: true,
            locked: false,
            ID: 1
        }, {
            title: 'Group 2',
            editable: true,
            locked: false,
            ID: 2
        }]
    }

    getByID(ID){
        return this.items.find(item => item.ID === ID) as Group;
    }
}