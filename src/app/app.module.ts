import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {HeaderComponent} from './header/header.component';
import {BreadcrumbsComponent} from './breadcrumbs/breadcrumbs.component';
import {CardListComponent} from './card-list/card-list.component';
import {CardListItemComponent} from './card-list-item/card-list-item.component';
import {GroupsListComponent} from './groups-list/groups-list.component';
import {GroupsListItemComponent} from './groups-list-item/groups-list-item.component';

import { routing, appRoutingProviders }  from './app.routing';


import {GroupsService} from './groups.service';
import {CardServiceService} from './card-service.service';
import {AppComponent} from './app.component';
import {BreadcrumbsService} from './breadcrumbs/breadcrumbs.service';

@NgModule({
    declarations: [
        HeaderComponent,
        BreadcrumbsComponent,
        CardListComponent,
        CardListItemComponent,
        GroupsListComponent,
        GroupsListItemComponent,
        AppComponent],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        routing
    ],
    providers: [GroupsService, CardServiceService, appRoutingProviders,BreadcrumbsService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
