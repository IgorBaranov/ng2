import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';

import {Group} from '../groups.interface'

@Component({
    selector: 'app-groups-list-item',
    templateUrl: './groups-list-item.component.html',
    styleUrls: ['./groups-list-item.component.css']
})
export class GroupsListItemComponent implements OnInit {
    @Input()
    item: Group;

    constructor(private router: Router) {

    }

    ngOnInit() {

    }

    selectGroup($event) {
        this.router.navigate(['/cards/', this.item.ID]);
        $event.preventDefault();
    }

}
