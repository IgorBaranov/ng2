import {Component, OnInit, Input, Pipe} from '@angular/core';

import {Cards} from '../card.interface';

@Component({
    selector: 'app-card-list-item',
    templateUrl: './card-list-item.component.html',
    styleUrls: ['./card-list-item.component.css']
})
export class CardListItemComponent implements OnInit {
    @Input() item: Cards;

    constructor() {
    }

    ngOnInit() {
    }

}
