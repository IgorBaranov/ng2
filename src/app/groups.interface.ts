export interface Group {
    title: string;
    editable: boolean;
    locked: boolean;
    ID: number;
}