import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardListComponent } from './card-list/card-list.component';

const appRoutes: Routes = [
    { path: 'cards/:id', component: CardListComponent },
    { path: 'cards/', component: CardListComponent },
    { path: '**', component: CardListComponent }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
